<?php
//equal
$a=5;
$b=5;
var_dump($a==$b);
echo"<hr>";

//not equal
$c=20;
$d=3;
var_dump($c!=$d);
echo "<hr>";

//identical
$e=5;
$f="50";
var_dump($e===$f);
echo "<hr>";

//not identical
$g="50";
$h=10;
var_dump($g!==$h);
echo "<hr>";

//greater than
$i=10;
$j=9;
var_dump($i>$j);
echo "<hr>";

//less than
$k=45;
$l=50;
var_dump($k<$l);
echo "<hr>";

//greater than equal
$m=24;
$n=24;
var_dump($m>=$n);
echo "<hr>";

//less than equal
$o=16;
$p=15;
var_dump($o<=$p);
echo "<hr>";

?>