
<?php
//AND
$x = 10;
$y = 5;

if ($x == 10 and $y == 5) {
    echo "Hello world!" ."<br>";
}


//OR

$x = 100;
$y = 50;

if ($x == 100 or $y == 80) {
    echo "Hy!" ."<br>";
}

//XOR
$x = 100;
$y = 50;

if ($x == 100 xor $y == 80) {
    echo "Good" ."<br>";
}

//NOT

$x = 100;

if ($x !== 90) {
    echo "Morning!" . "<br>";
}


//AND
$x = 10;
$y = 5;

if ($x == 10 && $y == 5) {
    echo "What!" . "<br>";
}


//OR
$x = 100;
$y = 50;

if ($x == 100 || $y == 80) {
    echo "World!";
}
?>
