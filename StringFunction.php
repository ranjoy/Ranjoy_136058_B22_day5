<?php
//addslashes
$str = addslashes('What is your name?');
echo($str) ."<hr>";
?>

<?php
//explode
$str = "Nothing is impossible.";
print_r (explode(" ",$str)) ;
echo "<hr>";
?>

<?php

//implode
$arr = array('Nothing','is','impossible');
echo implode(" ",$arr);
echo "<hr>";
?>

<?php
//str_shuffle

echo str_shuffle("Hello World");
echo "<hr>";
?>

<?php
//printf

$pen = 9;
$str = "Books";
printf("There are %u pen and four %s.",$pen,$str);
echo "<hr>";
?>

<?php
//trim

$str = "Hello World!";
echo $str . "<br>";
echo trim($str,"Held!");
echo "<hr>";
?>


<?php
//ltrim

$str = "Good Evening!";
echo $str . "<br>";
echo ltrim($str,"Good");
echo "<hr>";
?>

<?php

//rtrim
$str = "Good Evening!";
echo $str . "<br>";
echo rtrim($str,"Evening!");
echo "<hr>";
?>


<?php
//nl2br
echo nl2br("First line.\nSecond line.");
echo "<hr>";
?>

<?php
//str_pad
$str = "Hello World";
echo str_pad($str,20,"_");
echo "<hr>"
?>

<?php
//str_repeat

echo str_repeat("Hello",5);
echo "<hr>"
?>

<?php
//str_replace
echo str_replace("Morning","Night","Good Morning!");
echo "<hr>"
?>

<?php
//str_split
print_r(str_split("Hello"));
echo "<hr>";
?>

<?php
//strip _tags
echo strip_tags("Hello <h3>world!</h3>");
echo "<hr>";
?>

<?php
//strlen
echo strlen("Hello World");
echo "<hr>";
?>

<?php
//strtoupper
echo strtoupper("Hello WORLD!") ."<hr>";
?>

<?php
//strtolower
echo strtolower("Hello WORLD!") ."<hr>";
?>

<?php
//substr
echo substr("Hello world",4) ."<hr>";

?>

<?php
//substr_count
echo substr_count("I am a student.As a student, i am go to versity.....","student");
echo "<hr>";
?>

<?php
//ucwords
echo ucwords("good morning") ."<hr>";
?>

<?php
//ucfirst
echo ucfirst("the name of our country is Bangladesh.");
?>
